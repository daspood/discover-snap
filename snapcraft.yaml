
# SPDX-FileCopyrightText: 2024 Antoine Gonzalez <antoine.gonzalez@enioka.com>
#
# SPDX-License-Identifier: CC0-1.0

# FIXME's:
# - plasma-discover:
#       -> snapd-glib's snapd_snap_get_apps returns frequently returns an empty list
#       -> APPARMOR denied on `/proc/sys/kernel/core_pattern` file read
# - plasma-discover-notifier:
#       -> no snapd backend exists for notifier
#       -> APPARMOR denied on `/proc/sys/kernel/core_pattern` file read

---
name: plasma-discover
confinement: strict
grade: stable
base: core22
adopt-info: plasma-discover
apps:
    plasma-discover:
        environment:
            LD_LIBRARY_PATH: $SNAP/usr/lib/x86_64-linux-gnu/plasma-discover:$LD_LIBRARY_PATH
            _KDE_APPLICATIONS_AS_FORKING: 1
        extensions:
            - kde-neon-6
        common-id: org.kde.discover.desktop
        desktop: usr/share/applications/org.kde.discover.desktop
        command: usr/bin/plasma-discover
        slots:
            - dbus-kde-discover
        plugs:
            - desktop
            - wayland
            - x11
            - desktop-launch
            - snapd-control
            - screen-inhibit-control
            - network
            - network-status
            - network-manager
            - system-observe         # Needed to read /proc/cgroups
            - mount-observe          # Needed to read /proc/self/mountinfo
            - system-snap
            - home-snap
    plasma-discover-notifier:
        environment:
            LD_LIBRARY_PATH: $SNAP/usr/lib/x86_64-linux-gnu/plasma-discover:$LD_LIBRARY_PATH
            _KDE_APPLICATIONS_AS_FORKING: 1
        extensions:
            - kde-neon-6
        command: usr/lib/x86_64-linux-gnu/libexec/DiscoverNotifier
        daemon: simple
        passthrough:
            daemon-scope: user
        restart-delay: 1s
        slots:
            - dbus-kde-discover-notifier
        plugs:
            - desktop
            - wayland
            - x11
            - snapd-control
            - network
            - network-status
            - network-manager
            - system-snap
            - home-snap
assumes:
- snapd2.58.3
compression: lzo
slots:
    dbus-kde-discover:
        interface: dbus
        bus: session
        name: org.kde.discover
    dbus-kde-discover-notifier:
        interface: dbus
        bus: session
        name: org.kde.discover.notifier
plugs:
    system-snap:
        interface: system-files
        read:
            - /snap           # $SNAP
            - /var/snap       # $SNAP_DATA and $SNAP_COMMON
            - /var/lib/snapd  # Needed to read snap dir options
            - /usr/bin/snap   # Needed to run snap commands
            - /run/user       # Needed to read the Auth file of installed snaps
            - /etc/xdg/menus  # Needed for desktop-launch
            - /usr/share/applications/mimeapps.list  # Needed for desktop-launch
    home-snap:
        interface: personal-files
        read:
            - $HOME/.snap/auth.json
        write:
            - $HOME/snap      # Needed to access and create dirs for installed snaps
environment: {}
hooks: {}
layout: {}
package-repositories:
-   type: apt
    components:
    - main
    suites:
    - jammy
    key-id: 444DABCF3667D0283F894EDDE6D4736255751E5D
    url: http://archive.neon.kde.org/user
    key-server: keyserver.ubuntu.com
parts:
    plasma-discover:
        parse-info:
        - usr/share/metainfo/org.kde.discover.appdata.xml
        source: https://invent.kde.org/plasma/discover.git
        source-type: git
        source-branch: v6.0.5  # v6.0.80 onwards require ECM 6.2.0, kde-neon-6 extension only has 6.1.0
        plugin: cmake
        build-packages:
        - libsnapd-glib-dev
        - libsnapd-qt-dev
        - libappstreamqt-dev
        stage-packages:
        - libsnapd-glib1
        - libsnapd-qt1
        - libappstreamqt-3
        override-pull: |
            craftctl default
            # Apply patches
            git apply $CRAFT_PROJECT_DIR/patches/810-snap-backend-updates.patch
            git apply $CRAFT_PROJECT_DIR/patches/873-snap-backend-desktop-launch-BACKPORT.patch
            # Force-disable KNS backend which does not have an associated cmake variable
            sed -i '11,13d' $SNAPCRAFT_PART_SRC/libdiscover/backends/CMakeLists.txt
        cmake-parameters:
        - "--log-level=STATUS"
        - "-DBUILD_DummyBackend=OFF"
        - "-DBUILD_FlatpakBackend=OFF"
        - "-DBUILD_FwupdBackend=OFF"
        - "-DBUILD_RpmOstreeBackend=OFF"
        - "-DBUILD_SnapBackend=ON"
        - "-DBUILD_SteamOSBackend=OFF"
        - "-DBUILD_TESTING=OFF"
        - "-DBUILD_WITH_QT6=ON"
        - "-DBUILD_SHARED_LIBS=TRUE"
        - "-DCMAKE_BUILD_TYPE=Release"
        - "-DCMAKE_EXPORT_NO_PACKAGE_REGISTRY=ON"
        - "-DCMAKE_FIND_PACKAGE_NO_PACKAGE_REGISTRY=ON"
        - "-DCMAKE_FIND_ROOT_PATH=$CRAFT_STAGE\\;/snap/kde-qt6-core22-sdk/current\\;/snap/kf6-core22-sdk/current\\;/usr"
        - "-DCMAKE_FIND_USE_PACKAGE_REGISTRY=OFF"
        - "-DCMAKE_INSTALL_LIBDIR=lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR"
        - "-DCMAKE_INSTALL_LOCALSTATEDIR=/var"
        - "-DCMAKE_INSTALL_PREFIX=/usr"
        - "-DCMAKE_INSTALL_RUNSTATEDIR=/run"
        - "-DCMAKE_INSTALL_SYSCONFDIR=/etc"
        - "-DCMAKE_LIBRARY_PATH=lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR"
        - "-DCMAKE_PREFIX_PATH=$CRAFT_STAGE\\;/snap/kde-qt6-core22-sdk/current\\;/snap/kf6-core22-sdk/current\\;/usr"
        - "-DCMAKE_SKIP_INSTALL_ALL_DEPENDENCY=ON"
        - "-DCMAKE_VERBOSE_MAKEFILE=ON"
        - "-DENABLE_TESTING=OFF"
        - "-DKDE_INSTALL_FULL_APPDIR=$SNAP/usr/share/applications"
        - "-DKDE_INSTALL_PLUGINDIR=/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR/qt6/plugins/"
        - "-DKDE_INSTALL_USE_QT_SYS_PATHS=FALSE"
        - "-DKDE_SKIP_TEST_SETTINGS=ON"
        - "-DQT_MAJOR_VERSION=6"
        prime:
        - "-usr/lib/*/cmake/*"
        - "-usr/include/*"
        - "-usr/share/ECM/*"
        - "-usr/share/man/*"
        - "-usr/share/icons/breeze-dark*"
        - "-usr/bin/X11"
        - "-usr/lib/gcc/$CRAFT_ARCH_TRIPLET_BUILD_FOR/6.0.0"
        - "-usr/lib/aspell/*"
        - "-usr/share/lintian"
        - "-patches"
        build-environment:
        - PATH: /snap/kde-qt6-core22-sdk/current/usr/bin:/snap/kf6-core22-sdk/current/usr/bin${PATH:+:$PATH}
        - XDG_DATA_DIRS: $CRAFT_STAGE/usr/share:/snap/kde-qt6-core22-sdk/current/usr/share:/snap/kf6-core22-sdk/current/usr/share:/usr/share${XDG_DATA_DIRS:+:$XDG_DATA_DIRS}
        - XDG_CONFIG_HOME: $CRAFT_STAGE/etc/xdg:/snap/kde-qt6-core22-sdk/etc/xdg:/snap/kf6-core22-sdk/etc/xdg:/etc/xdg${XDG_CONFIG_HOME:+:$XDG_CONFIG_HOME}
        - LD_LIBRARY_PATH: /snap/kde-qt6-core22-sdk/current/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR:/snap/kf6-core22-sdk/current/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR:/snap/kde-qt6-core22-sdk/current/usr/lib:/snap/kf6-core22-sdk/current/usr/lib:$CRAFT_STAGE/usr/lib:CRAFT_STAGE/usr/lib:$CRAFT_STAGE/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR{LD_LIBRARY_PATH:+:LD_LIBRARY_PATH}
    cleanup:
        after:
        - plasma-discover
        plugin: nil
        build-snaps:
        - core22
        - kf6-core22
        - kde-qt6-core22
        override-prime: |
            set -eux
            for snap in "core22" "kf6-core22" "kde-qt6-core22"
            do
                cd "/snap/$snap/current" && find . -type f,l -exec rm -rf "$CRAFT_PRIME/{}" \;
            done
