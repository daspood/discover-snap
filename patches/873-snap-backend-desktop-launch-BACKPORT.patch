diff --git a/libdiscover/backends/SnapBackend/CMakeLists.txt b/libdiscover/backends/SnapBackend/CMakeLists.txt
index a3bb0f005..ecb1f763a 100644
--- a/libdiscover/backends/SnapBackend/CMakeLists.txt
+++ b/libdiscover/backends/SnapBackend/CMakeLists.txt
@@ -1,7 +1,23 @@
 add_subdirectory(libsnapclient)
 
-add_library(snap-backend MODULE SnapResource.cpp SnapBackend.cpp SnapTransaction.cpp snapui.qrc)
-target_link_libraries(snap-backend Qt::Gui Qt::Core Qt::Concurrent KF6::CoreAddons KF6::ConfigCore Discover::Common Snapd::Core)
+set(snap-backend_SRCS
+        SnapResource.cpp
+        SnapBackend.cpp
+        SnapTransaction.cpp
+        snapui.qrc
+)
+ecm_qt_declare_logging_category(snap-backend_SRCS HEADER libdiscover_snap_debug.h IDENTIFIER LIBDISCOVER_SNAP_LOG CATEGORY_NAME org.kde.plasma.libdiscover.snap DESCRIPTION "libdiscover snap backend" EXPORT DISCOVER)
+add_library(snap-backend MODULE ${snap-backend_SRCS})
+target_link_libraries(snap-backend
+    Qt::Gui
+    Qt::Core
+    Qt::Concurrent
+    KF6::CoreAddons
+    KF6::ConfigCore
+    KF6::KIOGui
+    Discover::Common
+    Snapd::Core
+)
 
 if ("${Snapd_VERSION}" VERSION_GREATER 1.40)
     target_compile_definitions(snap-backend PRIVATE -DSNAP_COMMON_IDS -DSNAP_CHANNELS)
diff --git a/libdiscover/backends/SnapBackend/SnapResource.cpp b/libdiscover/backends/SnapBackend/SnapResource.cpp
index 4374c8790..ca17b2425 100644
--- a/libdiscover/backends/SnapBackend/SnapResource.cpp
+++ b/libdiscover/backends/SnapBackend/SnapResource.cpp
@@ -6,9 +6,13 @@
 
 #include "SnapResource.h"
 #include "SnapBackend.h"
+#include "libdiscover_snap_debug.h"
+#include <KIO/ApplicationLauncherJob>
+#include <KService>
 #include <KLocalizedString>
 #include <QBuffer>
 #include <QDebug>
+#include <QFile>
 #include <QImageReader>
 #include <QProcess>
 #include <QStandardItemModel>
@@ -282,9 +286,62 @@ void SnapResource::fetchScreenshots()
     Q_EMIT screenshotsFetched(screenshots);
 }
 
+/**
+ * Tries to obtain a launchable desktop file for this snap, in this order:
+ *
+ * 1. Any app with the same name as the snap and a desktop file (the main app)
+ * 2. The first app with a desktop file (the next best app)
+ * 3. The expected desktop file for the main app
+ *
+ * @return The path to the selected launchable desktop file.
+ */
+QString SnapResource::launchableDesktopFile() const
+{
+    QString desktopFile;
+    qCDebug(LIBDISCOVER_SNAP_LOG) << "Snap: " << packageName() << " - " << m_snap->appCount() << " app(s) detected";
+    for (int i = 0; i < m_snap->appCount(); i++) {
+        if (m_snap->app(i)->desktopFile().isEmpty()) {
+            qCDebug(LIBDISCOVER_SNAP_LOG) << "App " << i << ": " << m_snap->app(i)->name() << ": " << "No desktop file, skipping";
+            continue;
+        }
+        if (packageName().compare(m_snap->app(i)->name(), Qt::CaseInsensitive) == 0) {
+            qCDebug(LIBDISCOVER_SNAP_LOG) << "App " << i << ": " << m_snap->app(i)->name() << ": " << "Main app, stopping search";
+            desktopFile = m_snap->app(i)->desktopFile();
+            break;
+        }
+        if (desktopFile.isEmpty()) {
+            qCDebug(LIBDISCOVER_SNAP_LOG) << "App " << i << ": " << m_snap->app(i)->name() << ": " << "First candidate, keeping for now";
+            desktopFile = m_snap->app(i)->desktopFile();
+        }
+    }
+    if (desktopFile.isEmpty()) {
+        qCWarning(LIBDISCOVER_SNAP_LOG) << "No desktop file found for this snap, trying expected path for the main app desktop file";
+        desktopFile = QStringLiteral("/var/lib/snapd/desktop/applications/")
+                      + packageName()
+                      + QStringLiteral("_")
+                      + packageName()
+                      + QStringLiteral(".desktop");
+    }
+    return desktopFile;
+}
+
+/**
+ * Launches a snap using its desktop file.
+ *
+ * If no desktop file is found, defaults back to `snap run`, which will fail in Ubuntu Core environments.
+ */
 void SnapResource::invokeApplication() const
 {
-    QProcess::startDetached(QStringLiteral("snap"), {QStringLiteral("run"), packageName()});
+    QString desktopFile = launchableDesktopFile();
+    if (QFile::exists(desktopFile)) {
+        qCDebug(LIBDISCOVER_SNAP_LOG) << "Launching desktop file " << desktopFile;
+        KService::Ptr service = KService::serviceByDesktopPath(desktopFile);
+        KIO::ApplicationLauncherJob *job = new KIO::ApplicationLauncherJob(service);
+        job->start();
+    } else {
+        qCWarning(LIBDISCOVER_SNAP_LOG) << "No desktop file found, defaulting to snap run";
+        QProcess::startDetached(QStringLiteral("snap"), {QStringLiteral("run"), packageName()});
+    }
 }
 
 AbstractResource::Type SnapResource::type() const
@@ -358,7 +415,7 @@ public:
                     // qDebug() << "xxx" << plug->name() << plug->label() << plug->interface() << slot->snap() << "slot:" << slot->name() <<
                     // slot->snap() << slot->interface() << slot->label();
                     item->setCheckable(true);
-                    item->setCheckState(plug->connectionCount() > 0 ? Qt::Checked : Qt::Unchecked);
+                    item->setCheckState(plug->connectedSlotCount() > 0 ? Qt::Checked : Qt::Unchecked);
                     item->setData(plug->name(), PlugNameRole);
                     item->setData(slot->snap(), SlotSnapRole);
                     item->setData(slot->name(), SlotNameRole);
diff --git a/libdiscover/backends/SnapBackend/SnapResource.h b/libdiscover/backends/SnapBackend/SnapResource.h
index 7a3a8abbd..b35ce5586 100644
--- a/libdiscover/backends/SnapBackend/SnapResource.h
+++ b/libdiscover/backends/SnapBackend/SnapResource.h
@@ -42,6 +42,7 @@ public:
     {
         return true;
     }
+    QString launchableDesktopFile() const;
     void invokeApplication() const override;
     void fetchChangelog() override;
     void fetchScreenshots() override;
